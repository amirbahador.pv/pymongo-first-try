from pymongo import MongoClient
import os


BASE_DIR = '/home/amirbahador/Desktop/work/python/mess with mongopy/pymongo-first-try/'
path = os.path.join(BASE_DIR, 'conff.txt')
with open(path) as f:
    config_info = f.readline()
client = MongoClient(config_info)
all_dbs = client.list_database_names()
print('my databases in mongo are {}'.format(all_dbs))
log_db = client.mongolog
collections = log_db.list_collection_names()
print('mongolog collections are {}'.format(collections))
f_post = {
    "_id": 7,
    "product":"Acer TravelMate P255 15.6 Laptop Intel Core i5-4210U 1.70GHz 4GB RAM 250GB HDD",
    "reason":"null brand",
    "time":"now",
}
posts =[
    {
        "product":"Apple MacBook Pro MUHP2 2019 - 13 inch Laptop With Touch Bar",
        "reason":"null brand",
        "time":"yesterday",
        "price":25
    },
    {
        "product":"Apple MacBook Pro MVVK2 2019 - 16 inch Laptop With Touch Bar",
        "reason":"exeption",
        "time":"now",
        "price":26
    },
    {
        "product":"ASUS ROG Strix G531GW - A - 15 inch Laptop",
        "reason":"exeption",
        "time":"now",
        "price":27
    },
    {
        "product":"Microsoft Surface Book 2- C - 15 inch Laptop",
        "reason":"unknown",
        "time":"3 years ago",
        "price":77
    }
]
db = client["products"]
collection = db["laptop"]
collection.delete_many({}) #delete all documents in colloction

first_insert = collection.insert_one(f_post)
all_dbs = client.list_database_names()
collections = db.list_collection_names()
print('i create products db lets check it ...')
print('my databases in mongo are {}'.format(all_dbs))
print('i create laptop collection lets check it ...')
print('mongolog collections are {}'.format(collections))
get_obj = collection.find_one({'_id':7}) 
print('this is my object ==> {}'.format(get_obj))
collection.insert_many(posts)
print('--------add a multipul products ----------')
results = collection.find({'reason':"null brand"})
for result in results:
    print(result)


collection.drop() #delete colloction
client.drop_database('products') #delete database